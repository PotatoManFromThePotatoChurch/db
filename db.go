package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"
)

type Credentials struct {
	host string
	user string
	name string
	port string
	pass string
}

type Command struct {
	echo        bool
	credentials Credentials
}

// This feels rather tedious.  Is there a more concise way to say "return this
// string or just blow up"?  Similarly, the `path` module doesn't seem to
// operate on path objects at all, but just contain a bunch of functions you
// apply to strings.  Is there a more powerful way to interact with paths in
// Go, perhaps similar to Python's pathlib?  I've seen this project:
//
//	https://github.com/chigopher/pathlib
//
// but it's not clear to me whether this is something that is commonly used and
// if I should be learning from this example.
//
// For comparison, in Python, this whole function would just be:
//
//	return Path.home() / ".pgpass"
//
// Realistically, it wouldn't even be a function.
func (command *Command) getPgpass() string {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("No home directory could be found: %s", err)
	}
	return path.Join(home, ".pgpass")
}

// Reach into `${HOME}/.pgpass` and given an expected format of:
//
//	# my-alias
//	Host:Username:DatabaseName:Port:Password
//
// return a map in the format:
//
//	{
//	    "my-alias": Credentials{host:Host, user:Username,...},
//	    ...
//	}
//
// If this were Python, I'd want to decorate this with @cached_property so it
// might be referenced as a property rather than a function call and the value
// would be cached in memory so we don't have to process the file more than
// once.
func (command *Command) getCredentials() map[string]Credentials {

	r := make(map[string]Credentials)

	// Open the file
	pgpass := command.getPgpass()
	file, err := os.Open(pgpass)
	if err != nil {
		log.Fatalf("Couldn't open %s: %s", pgpass, err)
	}
	defer file.Close()

	// I would have preferred to use named capture groups here instead of
	// indexed ones, but the docs make using them look even more complicated
	// than relying on indexes.  What's more, nearly all examples I see online
	// don't use named groups.  Is this just not a thing in Go, or should I be
	// using some other regex library for this?
	var aliasRegex = regexp.MustCompile(`^# (.*)$`)
	var credentialsRegex = regexp.MustCompile(`^([^:]+):(\w+):([^:]+):(\d+):(.+)$`)

	// Read the file line by line
	currentAlias := ""
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		line := strings.TrimSpace(scanner.Text())

		if line == "" {
			continue
		}

		if currentAlias == "" {
			alias := aliasRegex.FindStringSubmatch(line)
			if len(alias) > 0 {
				currentAlias = alias[1]
				continue
			}
		}

		if currentAlias != "" {
			cr := credentialsRegex.FindStringSubmatch(line)
			if len(cr) > 0 {
				r[currentAlias] = Credentials{
					cr[1],
					cr[2],
					cr[3],
					cr[4],
					cr[5],
				}
				currentAlias = ""
				continue
			}
		}

	}

	return r

}

// psql and pgcli have identical command line arguments so they're safe to
// use interchangeably.  If I add other options in the future though, this may
// need to be expanded into a class structure that knows how to invoke itself
// given a `Credentials` object.
func (command *Command) findBinary() string {
	pth, err := exec.LookPath("pgcli")
	if err != nil {
		pth, err = exec.LookPath("psql")
		if err != nil {
			log.Fatal("You must have either pgcli or psql installed for this to work.")
		}
	}
	return pth
}

func (command *Command) Init() error {

	aliases := command.getCredentials()

	echo := flag.Bool(
		"echo",
		false,
		"Show the command before executing it",
	)

	flag.Parse()

	command.echo = *echo

	// Because Go's flag module apparently doesn't have any means of validating
	// positional arguments, we need these separate checks.  I had expected to
	// find an API that looked something like this:
	//
	//     alias := flag.Text(position int, choices []string, explanation string)
	//
	// But everything I find in there appears to be exclusively for flags, so
	// I'm left doing this sort of thing manually.  Surely there's an easier
	// way?

	if flag.NArg() != 1 {
		return errors.New("Usage: db <alias>")
	}

	args := flag.Args()
	alias := args[0]
	cr, ok := aliases[alias]
	if !ok {
		return errors.New("no database alias by that name available")
	}
	command.credentials = cr

	return nil

}

func (command *Command) Connect() error {

	binary := command.findBinary()

	if command.echo {
		fmt.Printf(
			"%s --host %s --user %s --port %s %s (Password: %s)\n",
			binary,
			command.credentials.host,
			command.credentials.user,
			command.credentials.port,
			command.credentials.name,
			command.credentials.pass,
		)
	}

	// Despite the various Std* assignments below, pgcli doesn't run via this
	// wrapper exactly as it would if invoked directly.  For example, if I do
	// a large SELECT statement, I get this warning:
	//
	// WARNING: terminal is not fully functional
	//
	// It still loads the results, but the pager is non-functional, so I can't
	// pan through those results left/right.  I'm not sure what I'm missing
	// here, but I'm assuming it has something to do with a pty?
	cmd := exec.Command(
		binary,
		"--host", command.credentials.host,
		"--user", command.credentials.user,
		"--port", command.credentials.port,
		command.credentials.name,
	)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Env = []string{"PGPASSWORD=" + command.credentials.pass}

	err := cmd.Start()
	if err != nil {
		return errors.New(
			fmt.Sprintf("Error starting the session: %s\n", err),
		)
	}

	err = cmd.Wait()
	if err != nil {
		return errors.New(
			fmt.Sprintf(
				"Error waiting for the session to close: %s\n",
				err,
			),
		)
	}

	return nil

}

// This feels very messy to me.  In Python, you'd just do something like:
//
//	try:
//	    sys.exit(Command()())
//	except CommandError as e:
//	    sys.stderr.write(str(e))
//	    sys.exit(1)
//
// But here I have to check for error conditions at each stage, the first stage
// of which is just me applying validation for functionality the flags module
// doesn't offer: validation on positional arguments.  Is there a better way
// to do this?
func main() {

	command := new(Command)

	err := command.Init()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	err = command.Connect()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(2)
	}

	os.Exit(0)

}
