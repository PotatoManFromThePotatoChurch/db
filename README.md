# db

Connect to PostgreSQL quick & easy.


## Why this exists

Connecting to a PostgreSQL database is tedious.  Even with a `${HOME}/.pgpass`
file setup, you still have to type out the whole thing, and for some cloud
services, this process can get *gnarly*:

```shell
$ psql -U myuser -h some.rediculously.long.domain.name.something.tld databasename
```

As I hop into databases many times a day for work, I found myself writing a
bunch of Bash aliases to save keystrokes.  Eventually I broke down and wrote
something to make this process more generic and reusable.


## How to use it

`db` saves you the effort and makes configuring these connections easy.

Where before your `${HOME}/.pgpass` file looked like this:

```
some.rediculously.long.domain.name.something.tld:huey:people:5432:yeez_ie4Ooheeth1gaez9amee2cei8ah
some.other.long.domain.name.something.tld:dewy:products:5432:ku"cafegai8Achejeiy5ua4eish~iequ
some.other.again.long.domain.name.something.tld:louie:food:5432:ain8eiqu9poo1athae5Ba1iha-ngohl1
```

Now it looks like this:

```
# people
some.rediculously.long.domain.name.something.tld:huey:people:5432:yeez_ie4Ooheeth1gaez9amee2cei8ah

# products
some.other.long.domain.name.something.tld:dewy:products:5432:ku"cafegai8Achejeiy5ua4eish~iequ

# food
some.other.again.long.domain.name.something.tld:louie:food:5432:ain8eiqu9poo1athae5Ba1iha-ngohl1
```

PostgreSQL ignores the comments, but `db` treats them like aliases you can
reference to invoke connections:

```shell
$ db food
```

It'll check to see if you've got [pgcli](https://www.pgcli.com/) installed and
use that if it can, otherwise it'll fall back to psql and just log you in.
